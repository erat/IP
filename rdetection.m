cam=webcam;
cam.Resolution = '640x480';
B=snapshot(cam);

frames = 0;
while (1) 
    
      last1 = snapshot(cam);
      last = flip(last1,2);
      frames = frames + 1; 
      
      
      im1 = imsubtract(last(:,:,1), rgb2gray(last)); 
        figure(1)
        imshow(im1)
         
      im2 = medfilt2(im1, [3 3]);
        figure(2)
        imshow(im2)
      
      im3 = im2bw(im2,0.179); 
        figure(3)
        imshow(im3)
     
      im4 = bwareaopen(im3,200);
         figure(4)
         imshow(im4)
     
     bw = bwlabel(im4,4);
       figure(5)
       imshow(bw)
    
     s = regionprops(bw, 'BoundingBox', 'Centroid');  
   hold on
   for ro = 1:length(s)
        b = s(ro).BoundingBox; c = s(ro).Centroid;
        rectangle('Position',b,'EdgeColor','r','LineWidth',1)
       
    end  
   hold off
   figure(6)
   imshow(last)
end


