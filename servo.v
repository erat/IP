`timescale 10 ns / 100 ps
module servo (
    input clk,  
    input [1:0] command, 
    output control
    );  
    localparam MS = 24'h1e8480; //ms =20;
    localparam MIDDLE = 24'h24f90; 
    localparam RIGHT = 24'h30d40; 
    localparam LEFT = 24'h186a0; 
    
    reg [23:0] count;
    reg pulse;
    
    initial count = 0; 
    assign control = pulse; 

    always@(posedge clk) 
            count <= (count == MS) ? 0 : count + 1'b1; 

    always@(*) 	
        case(command)
            2'b01: pulse = (count <= RIGHT); 
            2'b00: pulse = (count <= LEFT); 
            default: pulse = (count <= MIDDLE);  
        endcase

endmodule